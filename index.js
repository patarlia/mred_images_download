'Use Strict';


const repliersImageDownloader = require("@ckpd/repliers-image-downloads");



exports.handler = async (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    const defaultBatchSize = 150;
    await repliersImageDownloader({
        updateQuery: `
        update ${process.env.POST_TABLE} 
        SET Photo_Count_Extra=?, Photo_Status = ?, updatedOn = ? WHERE mlsNumber = ?`,
        selectListingQuery: `SELECT 
                                    a. Photo_Count_Extra,
                                    a.mlsNumber
                                FROM  ${process.env.DB_TABLE} as a
                                WHERE a.Photo_Status IN(0,8) 
                                AND a.type = "lease"
                                ORDER by a.Photo_Status asc, a.Photo_Download desc
                                limit ${process.env.BATCH_SIZE || defaultBatchSize} ;`,
                                
        getImagesQuery: "SELECT * FROM "+process.env.DB_IMGTABLE +" WHERE ResourceRecordID = ? AND MlgCanView = 1 ORDER BY CAST(`Order` as SIGNED INTEGER) ASC;",
        setPhotoCountUsingRow: true,

    });

    return callback(null, {});
};     
  

 